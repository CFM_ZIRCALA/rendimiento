import os
import openpyxl
import time
from openpyxl.worksheet import Worksheet
from openpyxl.drawing.image import Image

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
REPORT_PATH = os.path.join(ROOT_DIR, 'test')
report_name = "test-" + time.strftime('%H') + ".xlsx"
report_file_path = os.path.join(REPORT_PATH, report_name)

logo = Image(os.path.join(ROOT_DIR, 'resources/logo_sti.png'))

wb = openpyxl.Workbook()
# write in log
sh: Worksheet = wb.get_active_sheet()
sh.merge_cells('A1:A5')
sh.add_image(logo, 'A1')
sh.column_dimensions['A'].width = 10
sh.column_dimensions['B'].width = 14
sh.column_dimensions['C'].width = 12

sh.column_dimensions['D'].width = 12
sh.column_dimensions['E'].width = 12
# sh.column_dimensions['F'].hidden=True
sh.column_dimensions['G'].width = 12
sh.merge_cells('c1:e1')
sh.append(["", "Nombre", "", "NAME--"])

sh.append(["", "Comienzo Turno", time.strftime("%H:%M:%S"), time.strftime("%d-%m-%y")])

sh.append(["", "Prom. op", "=AVERAGE(D:D)", "Suma Operacion en min:", "=SUM(D:D)/60", "=G3/G5"])

sh.append(["", "Prom. Espera ", "=AVERAGE(E:E)", "Suma Esperas en min:", "=SUM(E:E)/60", "=G4/G5"])
sh.append(["", "Prom. totales", "=AVERAGE(F:F)", "Suma Totales en min:", "=SUM(F:F)/60", "=G5/G5"])

sh.append(
    ["Hora", "Movimiento", "Tipo Carga", "T operacion", "T Espera", "T mov.", ])
wb.save(report_file_path)

from time import sleep
from pyModbusTCP.client import ModbusClient
import pyModbusTCP.utils as util
from App.logHandler.ErrorHandler import ErrorHandler


class Connector:
    """
    Clase general para las conexiones con el plc
    """

    def __init__(self, settings):
        for x in range(0, settings.check_times):
            ErrorHandler("Trying to connect")
            try:
                self.ip = settings.plc
                self.timer = settings.timer
                self.debug = settings.debug
                self.check_times = settings.check_times
                self.client = ModbusClient(host=self.ip, debug=False, auto_close=settings.auto_close,
                                           auto_open=settings.auto_open)
                self.client.open()
                if not self.client.is_open():
                    raise NoClientException
            except NoClientException:
                ErrorHandler("Connector failed, client error no: ", self.client.last_error())
                ErrorHandler("Client open: ", self.client.is_open())
                sleep(0.2)
                continue
            ErrorHandler("Connector created")
            return

    def connect(self):
        self.client.open()
        if self.client.is_open():
            return True
        return self.plc_alive()

    def plc_alive(self):
        # Comprueba la conexion al PLC durante 'seg' segundos
        self.client.open()
        for i in range(self.check_times):
            if self.client.is_open():
                ErrorHandler(" connected, connector 27")
                return True
            ErrorHandler("Attempting to connect")
            sleep(self.timer)
        ErrorHandler("Failed to connect")
        return False

    def disconnect(self):
        return self.client.close()

    def get_var(self, var):
        pass

    def update_vars(self, vars):
        pass

    @staticmethod
    def to_float(var):
        ret = [util.decode_ieee(f) for f in util.word_list_to_long(var)]
        return ret[0]


class NoClientException(Exception):
    ErrorHandler("No client connected/created")
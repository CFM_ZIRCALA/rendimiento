"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
import os, time

import openpyxl

from ..logHandler.ErrorHandler import ErrorHandler

from openpyxl.drawing.image import Image
from App.crane_operator import Operator


class Writer:
    def __init__(self, file, file_path):
        self.file = file
        self.file_path = file_path
        pass

    def write(self, movement="", container="", operation_time="", wait_time="", error=False):
        # Todo: Fix below codeline
        # TODO: write in log file when movement/operation is successfully wrote
        suma = 0
        if operation_time == -1 and wait_time == -1:
            suma = ""
            operation_time = ""
            wait_time = ""
        else:
            if operation_time == -1:
                suma = wait_time
                operation_time = ""
            elif wait_time == -1:
                wait_time = ""
                suma = operation_time
            else:
                suma = operation_time + wait_time

        sheet = self.file.get_active_sheet()
        if error:
            sheet.append([time.strftime('%H:%M:%S'), "Error/No valido"])
        else:
            sheet.append(
                [time.strftime('%H:%M:%S'), movement, container, operation_time, wait_time, suma])
        self.file.save(self.file_path)
        ErrorHandler("wrote on report")

    @staticmethod
    def create_file(report_file_path, ROOT_DIR, operator=Operator()):
        wb = None
        if not os.path.exists(report_file_path):
            try:
                logo = Image(os.path.join(ROOT_DIR, 'resources/logo_sti.png'))

                wb = openpyxl.Workbook()
                # write in log
                sh = wb.get_active_sheet()

                sh.append(["", "Nombre", operator.name, "", 'RUT: ', operator.rut, "Tarros/Hora:", "=3600/C3",
                           "Contenedores Movidos:",
                           '=COUNTIF(C:C,"20")+COUNTIF(C:C,"40")+COUNTIF(C:C,"45")+COUNTIF(C:C,"20-OH")+2*COUNTIF(C:C,"2x20")'])

                sh.append(["", "Comienzo Turno", time.strftime("%H:%M:%S"), time.strftime("%d-%m-%y"), "", "rend bruto",
                           "=3600/C5"])

                sh.append(
                    ["", "Prom. op", "=AVERAGE(D:D)", "", "Suma Operacion en min:", "", "=SUM(D:D)/60", "=G3/G5"])

                sh.append(
                    ["", "Prom. Espera ", "=AVERAGE(E:E)", "", "Suma Esperas en min:", "", "=SUM(E:E)/60",
                     "=G4/G5"])
                sh.append(
                    ["", "Prom. totales", "=AVERAGE(F:F)", "", "Suma Totales en min:", "", "=SUM(F:F)/60",
                     "=G5/G5"])

                sh.append([""])

                sh.append(
                    ["Hora", "Movimiento", "Tipo Carga", "T operacion", "T Espera", "T mov.", ])
                sh.append([""])
                sh['H3'].number_format = '0.00%'
                sh['H4'].number_format = '0.00%'
                sh.add_image(logo, 'A1')
                sh.merge_cells('A1:A5')
                sh.merge_cells('e3:f3')
                sh.merge_cells('e4:f4')
                sh.merge_cells('e5:f5')
                sh.column_dimensions['A'].width = 12
                sh.column_dimensions['B'].width = 14
                sh.column_dimensions['C'].width = 12

                sh.column_dimensions['D'].width = 12
                sh.column_dimensions['E'].width = 12
                sh.column_dimensions['F'].width = 12
                sh.column_dimensions['G'].width = 12
                sh.column_dimensions['H'].width = 12
                sh.merge_cells('c1:d1')

                wb.guess_types = False
                wb.save(report_file_path)

            except Exception as e:
                ErrorHandler(e,
                             "Writer create file, on try to open/create new excel report file on" + str(
                                 report_file_path + " "),
                             e)
        else:
            try:
                wb = openpyxl.load_workbook(report_file_path)
            except Exception as e:
                ErrorHandler(e,
                             "Writer create file, on try to open/create new excel report file on" + str(
                                 report_file_path + " "),
                             e)

        # TODO: Agregar primera linea con formato
        return wb

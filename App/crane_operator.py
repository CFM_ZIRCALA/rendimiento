import os
import sys
import tempfile
from itertools import cycle

"""
Por Reynaldo Palomino para StiPort @2019
<reynaldo.palomino.92@gmail.com>
"""


class Operator():
    def __init__(self, rut='99.999.999-Z', name='', fingers_indexes=[]):
        rut = rut.replace('-', '')
        rut = rut.replace('.', '')
        rut = rut.replace('k', 'K')
        self.rut = rut
        self.name = name
        self.fingers = fingers_indexes
        if self.fingers == []:
            self.fetch_index()

    @staticmethod
    def getAll(father):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        operators_file = open(file_path, 'r')
        lines = operators_file.readlines()
        operators_file.close()
        father.print('Pos  | Rut                    | Nombre')
        spacing = ''

        for line in lines:
            if line[0] != '#':
                lineElements = line.split('|')
                if lineElements[1] != '' and lineElements[1] != '\n':
                    if len(lineElements[0]) == 1:
                        indexSpacing = '       '
                    if len(lineElements[0]) == 2:
                        indexSpacing = '     '
                    if len(lineElements[0]) == 3:
                        indexSpacing = '   '
                    rut = Operator.reconstruct(lineElements[1])

                    try:
                        name = lineElements[2].replace('\n', '')
                    except:
                        name = ''
                    father.print(lineElements[0] + indexSpacing + rut + '       ' + name)

    @staticmethod
    def reconstruct(rut):
        rut = rut.split('\n')[0]
        if len(rut) == 9:
            return rut[:2] + '.' + rut[2:5] + '.' + rut[5:8] + '-' + rut[8]
        elif len(rut) == 8:
            return rut[:1] + '.' + rut[1:4] + '.' + rut[4:7] + '-' + rut[7] + '  '

    @staticmethod
    def ver_rut(rut):
        try:
            rut = rut.replace('-', '')
            rut = rut.replace('.', '')
            aux = rut[:-1]
            dv = rut[-1:]

            revertido = map(int, reversed(str(aux)))
            factors = cycle(range(2, 8))
            s = sum(d * f for d, f in zip(revertido, factors))
            res = (-s) % 11

            if str(res) == dv:
                return True
            elif dv == 'K' or dv == 'k' and res == 10:
                return True
            else:
                return False
        except:
            return False

    def save(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        file_path_temp = os.path.join(ROOT_DIR, 'operators.txt.temp')
        
        if not os.path.isfile(file_path):
            self.createFile()
        
        operators_file = open(file_path, 'r')
        lines = operators_file.readlines()
        operators_file.close()

        operators_file = open(file_path_temp, 'w')
        for rawline in lines:
            if rawline[0] != '#':
                line = rawline.split('\n')[0]
                if int(line.split('|')[0]) in self.fingers:
                    operators_file.write(line.split('|')[0] + '|' + self.rut + '|' + self.name + '\n')
                else:
                    operators_file.write(rawline)
            else:
                operators_file.write(rawline)
        
        operators_file.close()
        os.rename(file_path_temp, os.path.join(ROOT_DIR, file_path))
        print('after rename file')
        
    def delete(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        file_path_temp = os.path.join(ROOT_DIR, 'operators.txt.temp')
        
        if not os.path.isfile(file_path):
            self.createFile()
        
        operators_file = open(file_path, 'r')
        lines = operators_file.readlines()
        operators_file.close()

        operators_file = open(file_path_temp, 'w')
        for rawline in lines:
            line = rawline.split('\n')[0]
            try:
                splitted_line = line.split('|')
                if self.rut in splitted_line:
                    operators_file.write(splitted_line[0] + '|\n')
                else:
                    operators_file.write(rawline)
            except:
                operators_file.write(rawline)
        operators_file.close()
        os.rename(file_path_temp, file_path)

    def fetch_index(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        operators_file = open(file_path, 'r')
        lines = operators_file.readlines()
        operators_file.close()

        for rawline in lines:
            try:
                if rawline.split('|')[1].split('\n')[0] == self.rut:
                    self.fingers.append(int(rawline.split('|')[0]))
            except:
                pass

    @staticmethod
    def exists(rut):
        rut = rut.replace('-', '')
        rut = rut.replace('.', '')
        if rut == '':
            return False

        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        operators_file = open(file_path, 'r')
        lines = operators_file.readlines()
        operators_file.close()

        for rawline in lines:
            try:
                if rawline.split('|')[1].split('\n')[0] == rut:
                    return True
            except:
                pass
        return False

    def complete_operator(self, finger_id):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        print(file_path)
        operators_file = open(file_path, 'r')
        lines = operators_file.readlines()
        operators_file.close()
        print("finger id->", finger_id, lines)
        
        # Obtains the operator's name and rut
        for rawline in lines:
            print(rawline)
            if rawline.find("#") == -1:
                try:
                    print(rawline.replace('\n','').split('|')[0], "f_id ", finger_id)
                    if rawline.replace('\n','').split('|')[0] == str(finger_id):
                        print('found finger line 170')
                        self.rut = rawline.replace('\n','').split('|')[1]
                        self.name = rawline.replace('\n','').split('|')[2]
                        print("el rut->", self.rut, " el nombre:", self.name)
                        break
                except IndexError as e:
                    print(str(e))
                    pass
                print(rawline)
            
        if self.rut != "":
            for rawline in lines:
                if rawline.find("#") == -1:
                    try:
                        if rawline.split('|')[1] == self.rut:
                            print("in 180")

                            self.fingers.append(int(rawline.split('|')[0]))
                            self.rut = rawline.replace('\n','').split('|')[1]
                            self.name = rawline.replace('\n','').split('|')[2]
                    except IndexError:
                        continue
        print("final print", "rut:", self.rut, "  name:", self.name, "___Fingers->", self.fingers)

    def contain_finger(self, finger):
        print('contains finger: ', finger in self.fingers)
        return finger in self.fingers
    
    def createFile(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        file_path = os.path.join(ROOT_DIR, 'operators.txt')
        file_path_temp = os.path.join(ROOT_DIR, 'operators.txt.temp')
        
        operators_file = open(file_path_temp,'w')
        operators_file.write('# No modificar el contenido de este archivo por NINGUN motivo\n')
        operators_file.write('# A menos que sea explicitamente necesario\n')
        operators_file.write('# El formato es bastante sencillo de todas formas\n')

        for index in map( str, range(0, 300)):
            operators_file.write(index+'|\n')
        operators_file.close()
        os.rename(file_path_temp, file_path)
"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
import queue
from App.led_app import Led
import threading
import sys
import os
import time
from .operationDetector.spreader import Spreader
from .operationDetector.detector import Detector
from . import settings
from App.logHandler.ErrorHandler import ErrorHandler
from App.reportModule.reporter import Writer
from App.crane_operator import Operator


class Handler(threading.Thread):
    def __init__(self, gui_queue, ROOT_DIR, LOG_DIR_PATH):
        threading.Thread.__init__(self)
        self.gui_queue = gui_queue
        self.ROOT_DIR = ROOT_DIR
        self.LOG_DIR_PATH = LOG_DIR_PATH

    def run(self):
        print("Detector started running")
        ROOT_DIR = self.ROOT_DIR
        LOG_DIR_PATH = self.LOG_DIR_PATH
        led_queue = queue.Queue()
        led_thread = Led(led_queue)
        led_thread.daemon = True
        led_thread.start()
        try:

            CONFIG_PATH = os.path.join(ROOT_DIR, 'config.ini')  # requires `import os`#

            finger_path = os.path.join(ROOT_DIR)
            tool_settings = settings.Settings(CONFIG_PATH)
            anon_spreader = Spreader(tool_settings)
            is_anon = -1
            led_queue.put(-1)
            sys.stdout.close()
            # Gracias a esto corre para siempre, muajaja
            sys.stdout.close()
            while True:
                operator = Operator("Anon", "Anon")
                LOG_PATH = os.path.join(LOG_DIR_PATH, time.strftime('%Y-%m-%d') + '.log')
                sys.stdout = open(LOG_PATH, 'a')
                # Pregunto si hay archivo del scanner
                if os.path.isfile(
                        os.path.join(ROOT_DIR, 'import.fpnt')) or tool_settings.is_debug() or is_anon is True:

                    if tool_settings.is_debug():
                        REPORT_PATH = os.path.join('/share/reportes', 'test')
                        report_name = "test" + time.strftime('%H-%M') + ".xlsx"
                        new_session = "Debug"
                        is_anon = -1
                        ErrorHandler("debug mode")
                        operator = Operator()

                    # desktop = os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop')
                    # Lo de arriba es una funcion para llevar al escritorio, por si se quiere dejar fijo alli
                    # TODO: hacer esta ruta dinamica
                    # Cuando reconoce una huella para iniciar
                    elif os.path.isfile(os.path.join(ROOT_DIR, 'import.fpnt')):
                        file = open(os.path.join(ROOT_DIR, 'import.fpnt'), 'r')
                        new_session = int(file.read())
                        # Instancio un nuevo operador y se completa buscando mediante la huella recien reconocida
                        file.close()
                        operator = Operator("", "", [new_session])
                        operator.complete_operator(new_session)

                        os.remove(os.path.join(ROOT_DIR, 'import.fpnt'))
                        ErrorHandler("****Finger id: ", new_session, " recognized****")
                        ErrorHandler("Session: ", str(new_session))
                        is_anon = False

                        # Crea la carpeta+archivo de reporte
                        if not os.path.isdir(os.path.join('/share', 'reportes')):
                            os.mkdir(os.path.join('/share', 'reportes'))
                            ErrorHandler('Report dir created')
                        # REPORT_PATH = os.path.join(ROOT_DIR, 'reportes/' + operator.name)
                        REPORT_PATH = os.path.join('/share/reportes/' + operator.name)
                        report_name = 'id-' + operator.name.replace(" ", "") + time.strftime('-%d-%m') + '.xlsx'

                    else:
                        is_anon = True
                        REPORT_PATH = os.path.join('/share/reportes', 'reportes-anonymous')
                        report_name = 'id-anonymous' + time.strftime('-%d-%m--%H-%M') + '.xlsx'
                        operator.name = "Anon"
                        operator.rut = "Anon"

                    if not os.path.exists(REPORT_PATH):
                        os.mkdir(REPORT_PATH)
                        ErrorHandler("***Report dir created")

                    report_file_path = os.path.join(REPORT_PATH, report_name)

                    wb = Writer.create_file(report_file_path, ROOT_DIR, operator)

                    # Los parametros de reset ser el tipo 0-> instruccion
                    # del 1 al 4 deben ser: id, nombre, inicio de turno, numero de grua
                    self.gui_queue.put(
                        ['session', operator.rut, operator.name, time.strftime('%H:%M %d/%m/%Y'), tool_settings.grua])
                    self.gui_queue.put(
                        ['log',
                         time.strftime(
                             '%Y-%m-%d %H:%M:%S') + "> Iniciando sesion: " + operator.name + ' de rut: ' + operator.rut])
                    print(operator.rut, operator.name, operator.fingers)

                    if wb is not None:
                        led_queue.put(is_anon)
                        writer = Writer(wb, report_file_path)
                        ErrorHandler("Writer created")

                        operation_detector = Detector(tool_settings, writer, finger_path, operator)

                        if operation_detector is not ErrorHandler:
                            if operation_detector.detect(self.gui_queue) is ErrorHandler:
                                led_queue.put(-1)
                                self.gui_queue.put(
                                    ['log',
                                     time.strftime("%Y-%m-%d %H:%M:%S") + "> : Error de lectura, sesion cerrada"])
                                led_queue.put(-1)
                            else:
                                self.gui_queue.put(['log', time.strftime(
                                    "%Y-%m-%d %H:%M:%S") + "> Tiempo de espera superior a fin de turno, o cierre de sesión"])

                        self.gui_queue.put(['log', time.strftime("%Y-%m-%d %H:%M:%S") + "> Sesion cerrada"])

                        sys.stdout.close()
                        sys.stdout = open(LOG_PATH, 'a')
                    else:
                        ErrorHandler("wb is none", "report file not created on init run",
                                     "check file name, tool settings/parameters or writer on report module")
                        led_queue.put(is_anon)
                        operator = None
                    anon_spreader = Spreader(tool_settings)
                    is_anon = False
                else:
                    if anon_spreader.connection is not ErrorHandler:
                        if anon_spreader.get_movement() in range(1, 2):
                            is_anon = True
                            led_queue.put(is_anon)
                            ErrorHandler("***Movement detected, starting anon session***")
                            self.gui_queue.put(['log', time.strftime(
                                "%Y-%m-%d %H:%M:%S") + "> : ***Movimiento detectado, iniciado sesion anonima***"])

                        else:
                            led_queue.put(-1)
                    else:
                        is_anon = -1
                        led_queue.put(-1)
                sys.stdout.close()

        except KeyboardInterrupt:
            Led.clean()
            sys.stdout.close()

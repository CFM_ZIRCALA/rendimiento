"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from configparser import ConfigParser

class Settings:
    # Agrega las configuraciones a un objeto Settings, para el manejo en distintas partes del programa
    def __init__(self, settings_path):
        # TODO: try-catch here, and write into log file
        # Parses the config.ini into an dictionary
        parameters = settings_parser(settings_path)

        self.plc = parameters["IP_PLC"]["ip_plc"]
        self.timer = to_float(parameters["TIMER"]["sleep_timer"]) / 1000
        self.sp_addr = to_int(parameters['SP_ADDR'])
        self.gr_addr = to_int(parameters['GR_ADDR'])
        self.auto_close = is_true(parameters["ETC"]["auto_close"])
        self.auto_open = is_true(parameters["ETC"]["auto_open"])
        self.debug = is_true(parameters["ETC"]["debug"])
        self.check_times = int(to_float(parameters["ETC"]["checkconntimes"]))
        self.deltas = to_int(parameters["DELTAS"])
        self.grua = parameters['ETC']['grua']

        data_types = parameters["DATA_TYPES"]
        # Transformar los valores discretos en true dentro del diccionario de los tipos de datos
        for key in data_types:
            data_types[key] = is_true(data_types[key])
        # Si la key de sp_addr o gr_addr no tiene tipo de dato definido, se setea en booleano
        for key in self.sp_addr.keys():
            if key not in data_types.keys():
                data_types[key] = False
        for key in self.sp_addr.keys():
            if key not in data_types.keys():
                data_types[key] = False

        self.data_types = data_types
        if "log_file_path" in parameters["ETC"].keys():
            if "log_file_name" in parameters["ETC"].keys():
                pass
            else:
                pass
        else:
            pass

        # print(self.sp_addr)
        # print(is_true(parameters["ETC"]["auto_close"]))

    def is_debug(self):
        return self.debug is True


def to_int(var):
    for key in var:
        if var[key] is '':
            var[key] = 0
        # este else es para validar las direcciones de memoria, ojo en el config de no pasar por direcciones de memoria negativas
        # else:
        #     if to_float(var[key]) < 0:
        #         var[key] = '0'
        var[key] = to_float(var[key])
    return var


def to_float(var):
    if var == "":
        return 0
    try:
        ret = float(var)
    except ValueError:
        ret = 150
    return ret


def is_true(var):
    return var in ["true", "TRUE", "1", "yep", "yass"]


def settings_parser(settings_path):
    # se incializa el controlador
    config = ConfigParser()

    # Abre el archivo de configuraciones
    config.read(settings_path)

    settings = {}
    # print(type(settings[0][0])) es un STR, conf sections es una lista de str
    for section in config.sections():
        settings[section] = {}
        for option in config.options(section):
            settings[section][option] = config.get(section, option)

    return settings
    # print(config.items(config.sections()[0]))

"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
# solicita data a hoist y spreader, luego interpreta.
#    deberia mantener el histroricoo de los ultimos (por definir) movimientos para detectar un tipo de operacion
#   al momento de detectar un cambio
#
from App.logHandler.ErrorHandler import ErrorHandler as Errors
from time import sleep, time, strftime
import os
from .grua import Grua
from .spreader import Spreader


class Detector:

    def __init__(self, settings, writer, finger_path, operator):
        """

        :param settings: settings instance.
        :param writer: excel file to write on.
        """
        try:
            self.operator = operator
            self.sequence = []
            self.spreader = Spreader(settings)
            self.grua = Grua(settings)
            self.sleep_timer = settings.timer
            self.writer = writer
            self.finger_path = finger_path
            Errors("Detector created")
            if self.spreader.connection is Errors or self.grua.connection is Errors:
                raise Errors
        except Errors:
            Errors("Fail create connectors in detector __init__")
            self.connection = Errors

    def detect(self, gui_queue):

        spreader = self.spreader
        grua = self.grua
        container = ""
        waiting_time = -1
        embarque = -1  # Todo: evaluar necesidad de esta
        spreader.get_state()
        if grua.refresh() is Errors and self.operator.rut == "Anon":
            return Errors
        sp_temp = -1
        prt = ""

        if spreader.locked:
            sp_last_mv = 2
        else:
            sp_last_mv = 1

        while grua.end_turn() is False:
            if grua.is_synchro():
                if grua.refresh() is Errors or spreader.detect() is Errors:
                    return Errors

                if os.path.isfile(os.path.join(self.finger_path, 'import.fpnt')):
                    file = open(os.path.join(self.finger_path, 'import.fpnt'), 'r')
                    new_session = int(file.read())
                    file.close()
                    if self.operator.contain_finger(new_session):
                        Errors("same operator detected, session closed")
                        os.remove(os.path.join(self.finger_path, 'import.fpnt'))
                        gui_queue.put(['log', strftime("%Y-%m-%d %H:%M:%S") + "> Huella reconocida en operador"])
                        return
                    elif self.operator.rut == "Anon":
                        Errors("fingerpint id: ", new_session, " detected, nn session closed")
                        return Errors
                        # Close session and return to tool waiting, do not forget to close file...
                    else:
                        gui_queue.put('log', "Huella no reconocida, cerrando sesión de todas formas")
                        return
                        # os.remove(os.path.join(self.finger_path, 'import.fpnt'))

                if (not grua.boom_h() and grua.control_on()) or (grua.boom_h() and not grua.control_on()):
                    spreader.reset_times()
                    # comentado lo de abajo por si es lo que hace fallar el cierre por espera
                    # if not grua.boom_h():
                    #     grua.wait_reset()
                    continue

                if grua.falla():
                    self.writer.write("FALLA")
                    Errors("FALLA, session is closed")
                    return Errors

                timer = time()

                sp_curr_mv = spreader.curr_mv()
                if sp_curr_mv != sp_temp:
                    # si traba
                    sp_temp = sp_curr_mv
                    if sp_curr_mv == 1 and sp_curr_mv != sp_last_mv:
                        grua.t_lock()
                        container = spreader.get_container_type()
                        if grua.is_tapa():
                            container = "tapa"
                        elif grua.is_over_high():
                            container += "-OH"
                        sp_last_mv = sp_curr_mv

                        prt = "********trabado"

                    elif sp_curr_mv == 2 and sp_curr_mv != sp_last_mv:
                        sp_last_mv = sp_curr_mv

                        if grua.is_tapa():
                            container = "tapa"
                            grua.wait_reset()
                        elif grua.is_over_high():
                            container += "-OH"
                        waiting_time = grua.wait_time

                        if grua.sy_tl() is True:

                            if grua.sy():
                                if grua.delta_gantry():
                                    # print("Reestiva: tiempo -> blabla")
                                    if spreader.get_total_time() == 0:
                                        waiting_time = 0
                                    self.writer.write("Reestiba", container, spreader.get_total_time())
                                    gui_queue.put(['log', strftime("%Y-%m-%d %H:%M:%S") + "> : reestiba"])
                                    prt = "********reestiba"
                                # Si no hay deltas, caso nulo
                            else:
                                # destrabado en tierra desde mar: desembarque
                                embarque = False
                                prt = "********desembarque"
                                if spreader.get_total_time() == 0 or spreader.get_total_time() < 0:
                                    waiting_time = 0
                                self.writer.write("Desemarque", container, spreader.get_total_time() - waiting_time,
                                                  waiting_time)
                                gui_queue.put(['demb' + container, time(), 'desembarque', container,
                                               spreader.get_total_time() - waiting_time, waiting_time])

                                gui_queue.put(
                                    ['log', strftime("%Y-%m-%d %H:%M:%S") + "> : demb" + container])

                                # print("Destrabado: Desembarque, espera: ", waiting_time)

                        elif grua.sy_tl() is False:

                            if grua.sy() is True:
                                # Destrabado en mar, desde tierra: embarque
                                prt = "********embarque"
                                # print("Destrabado: Embarque, espera: ", waiting_time)
                                if spreader.get_total_time() == 0 or spreader.get_total_time() < 0:
                                    waiting_time = 0
                                self.writer.write("Embarque", container, spreader.get_total_time() - waiting_time,
                                                  waiting_time)

                                gui_queue.put(["emb" + container, time(), 'embarque', container,
                                               spreader.get_total_time() - waiting_time, waiting_time])

                                gui_queue.put(['log', strftime("%Y-%m-%d %H:%M:%S") + "> : emb" + container])

                            elif grua.sy() is False:
                                # Trabado y destrabado en tierra
                                # Pre-operativo
                                # Si es pre operativo, resetear los timers de espera
                                # TODO: Print tiempos del pre operativo??
                                embarque = False
                                prt = "********operativo"

                                self.writer.write("Pre-operativo/mov en tierra", "----", "---", "---")
                                # print("Destrabado: pre-operativo, espera(?): ", waiting_time)
                        grua.wait_reset()
                        grua.clear_tl()

            # else:
            #     print("--- --- ---")
            # print(container)
            # print(prt)
            # prt = "**"
            # print("Wait: ", waiting_time)
            # print(time() - timer)
            else:
                if os.path.isfile(os.path.join(self.finger_path, 'import.fpnt')):
                    file = open(os.path.join(self.finger_path, 'import.fpnt'), 'r')
                    new_session = int(file.read())
                    file.close()
                    if self.operator.contain_finger(new_session):
                        Errors("same operator detected, session closed")
                        os.remove(os.path.join(self.finger_path, 'import.fpnt'))
                        gui_queue.put(['log', strftime("%Y-%m-%d %H:%M:%S") + "> Huella reconocida en operador"])
                        return 1
                    elif self.operator.rut == "Anon":
                        Errors("fingerpint id: ", new_session, " detected, nn session closed")
                        return Errors
                        # Close session and return to tool waiting, do not forget to close file...
                    else:
                        gui_queue.put('log', "Huella no reconocida, cerrando sesión")
                        # os.remove(os.path.join(self.finger_path, 'import.fpnt'))
                        return
            sleep(self.sleep_timer)
        self.writer.write("Cierre de sesion por espera en parking con boom arriba")
        gui_queue.put(
            ['log', strftime("%Y-%m-%d %H:%M:%S") + "> : Cierre de sesion por espera en parking con boom arriba"])
        return 1

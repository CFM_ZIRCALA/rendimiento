"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from time import time as clock

from App.logHandler.ErrorHandler import ErrorHandler as Errors
from App.plcHandler.operationDetector.gruaConnector import Connector


class Grua:
    """
    Clase que se encarga del manejo de los datos de la grua.
    """

    def __init__(self, settings):
        """
        Constructor de la clase, inicia una conexion, e inicia los tiempos en -1

        :param settings: Configuraciones cargadas desde config.ini
        """
        try:
            self.connection = Connector(settings)

            self.var_state = {"t_lock": {}, 'delta': {}, 'curr': {}}
            self.deltas = {}
            for key in settings.gr_addr:
                # Contains previous and current variable's values
                self.var_state["t_lock"][key] = -1
                self.var_state["curr"][key] = -1
            for key in settings.deltas.keys():
                self.var_state["delta"][key] = False
                self.deltas[key] = settings.deltas[key]
            self.waiting = False
            self.wait_time = -1
            self.wait_start = -1
            self.boom_start = -1
            self.gantry_t0 = -1
            self.actual_wait = 0
            Errors("Grua detector created")
            if self.connection.client is Errors:
                raise Errors
        except Errors:
            Errors("Fail create connector")
            self.connection = Errors

    def is_over_high(self):
        if self.var_state['curr']['hoist'] is not Errors and self.deltas['over_high'] is not Errors:
            return self.var_state['curr']['hoist'] > self.deltas['over_high'] and not self.sy()
        return False

    def is_synchro(self):
        if self.var_state['curr']['sync'] is not Errors:
            return self.var_state['curr']['sync']
        Errors('***Trolley/Hoist no sincronizados***')
        return False

    def is_tapa(self):
        if self.var_state['curr']['trolley'] < self.deltas['tapa'] or self.var_state['t_lock']['trolley'] < \
                self.deltas['tapa']:
            return True
        return False

    def gantry(self, gantry):
        if gantry:
            if self.gantry_t0 == -1:
                self.gantry_t0 = clock()
        else:
            if self.gantry_t0 == -1:
                return
            elif abs(self.gantry_t0 - clock()) > self.deltas['gantry']:
                self.var_state['delta']['gantry'] = True
            else:
                self.gantry_t0 = -1
                return

    def trolley(self):
        if self.var_state['curr']['trolley'] is Errors:
            return Errors
        elif self.var_state['curr']['trolley'] is int:
            if not self.var_state['delta']['trolley'] and self.var_state['curr']['trolley']:
                if -self.deltas['trolley'] > self.var_state['curr']['trolley'] - self.var_state['t_lock']['trolley'] > \
                        self.deltas['trolley']:
                    self.var_state['delta']['trolley'] = True

    def hoist(self):
        if self.var_state['curr']['hoist'] is Errors:
            return Errors
        elif self.var_state['curr']['hoist'] is int:
            if not self.var_state['delta']['hoist'] and self.var_state['delta']['hoist'] is not Errors:
                if abs(self.var_state['curr']['hoist'] - self.var_state['t_lock']['hoist']) > self.deltas['hoist']:
                    self.var_state['delta']['hoist'] = True

    def gantry_reset(self):
        self.var_state['delta']['gantry'] = False

    def trolley_reset(self):
        self.var_state['delta']['trolley'] = False

    def hoist_reset(self):
        self.var_state['delta']['hoist'] = False

    def sy_tl(self):
        return self.var_state["t_lock"]["sy"] is True

    def sy(self):
        return self.var_state['curr']['sy'] is True

    def control_on(self):
        if self.var_state['curr']['control_on'] is not Errors:
            return self.var_state['curr']['control_on']
        else:
            print('Control on is error')
        return False

    def is_waiting(self):
        return self.waiting

    def delta_gantry(self):
        return self.var_state['delta']['gantry'] is True

    def delta_hoist(self):
        return self.var_state['delta']['hoist'] is True

    def delta_trolley(self):
        return self.var_state['delta']['trolley'] is True

    def delta_mv_reset(self):
        for key in self.var_state["delta"].keys():
            self.var_state["delta"][key] = False
        self.gantry_reset()
        self.hoist_reset()
        self.trolley_reset()

    def t_lock(self):
        if not check_state_error(self.var_state["curr"]):
            self.var_state["t_lock"] = self.var_state["curr"].copy()
        #  self.sy_tl = self.var_state["t_lock"]["sy"]

    def clear_tl(self):
        for key in self.var_state['t_lock'].keys():
            self.var_state["t_lock"][key] = -1
        self.delta_mv_reset()

    def boom_h(self):
        return self.var_state['curr']['boom']

    def wait_reset(self):
        self.waiting = False
        self.wait_time = 0
        self.wait_start = -1

    def wait_boom(self):
        self.waiting = False
        self.wait_time = 0
        self.wait_start = -1

    def wait(self, drive_0=0):
        # Solo reviso esto si estoy en tierra
        if not self.sy() or self.var_state['curr']['trolley'] < self.deltas['waiting_trolley']:
            if drive_0 == 1:
                if not self.waiting:
                    if self.wait_start == -1:
                        # si no hay comienzo de espera, la inicializo
                        self.wait_start = clock()
                    # Si la diferencia es mayor al delta establecido indico que esta esperando
                    if abs(clock() - self.wait_start) > self.deltas['waiting_time']:
                        self.waiting = True
                else:
                    # Esta linea deberia arreglar el problema del cierre de sesion
                    self.actual_wait = abs(clock() - self.wait_start)
            elif drive_0 == 0:
                # Si no esta esperando o el delta es menor al establecido reinicio el comienzo de espera
                if not self.waiting or abs(clock() - self.wait_start) < self.deltas['waiting_time']:
                    self.wait_start = -1
                # Si esta esperando(re compruebo la dif) sumo el tiempo al total
                elif self.waiting and abs(clock() - self.wait_start) > self.deltas[
                    'waiting_time'] and self.wait_start != -1:
                    self.wait_time += abs(clock() - self.wait_start)
                self.waiting = False
                self.actual_wait = 0

        else:
            # Si estoy en posicion no deberia correr la marca temporal
            self.wait_start = -1
            self.waiting = False
            self.actual_wait = 0

    def end_turn(self):
        # print de debug
        # print("__is waiting >", self.is_waiting(), "__wait time >", self.wait_time, "__end delta > ",
        #       self.deltas['end_turn'], "__boom >", self.boom_h(), "__control on >", self.control_on())

        if self.var_state['curr']['trolley'] is Errors or self.deltas['parking_trolley'] is Errors:
            return False

        # elif self.deltas['parking_trolley'] is float and self.var_state['curr']['hoist'] is float:

        if abs(self.var_state['curr']['trolley'] - self.deltas['parking_trolley']) < 0.51:
            print("__is waiting >", self.is_waiting(), "__wait time/(min) >", self.actual_wait / 60, "__end delta > ",
                  self.deltas['end_turn'], "__boom >", self.boom_h(), "__control on >", self.control_on(),
                  "__trolley pos >", self.var_state['curr']['trolley'])

            if self.is_waiting() and (self.actual_wait / 60) > self.deltas['end_turn'] and not self.boom_h() \
                    and not self.control_on():
                return True
        return False

    def falla(self):
        if self.var_state['curr']['falla'] is not Errors:
            return self.var_state['curr']['falla']
        return False

    def refresh(self):
        """
        Refresca/actualiza los momentos almacenados

        :return: True|False si reconoce un tiempo valido y distinto al actual
        """
        temp = {}
        flag = False  # flag avisa si es valido copiar o no el temporal al curr, y desplazar los anteriores
        for key in self.var_state["curr"].keys():
            temp[key] = self.get_var_state(key)
            if temp[key] == -1:
                flag = False
                break
            elif temp[key] != self.var_state['curr'][key]:
                flag = True
        # Si encuentra un una diferencia entre el estado actual de las variables,
        # y lo leido al refrescar, almacena el estado y desplaza los anteriores
        # -1 se usa como segnal de error, por lo que si lo encuentra no actualiza

        # Si es valido copiar, actualizo los valores

        if flag is True:
            self.var_state["curr"] = temp.copy()
        # Si no hay instrucciones, compruebo si esta el cronometro andando
        # Si hay instrucciones
        self.wait(self.var_state['curr']['drive_0'])
        if self.check_deltas() is Errors:
            return Errors

        # print("t_lock: ", self.var_state["t_lock"], '\nDelta: ', self.var_state['delta'], "\ncurrent: ",
        #       self.var_state['curr'])
        return flag

    def get_var_state(self, var):
        if self.connection is Errors:
            return -1
        else:
            put = self.connection.get_var(var)
            if put is not Errors and not None:
                if var in self.var_state['curr'].keys():
                    return put
                else:
                    Errors(None, "grua: get_var_state", "var:" + var + " not in var_sate[curr] keys")
                    return -1
            else:
                return -1

    def check_deltas(self):
        if not self.delta_gantry():
            self.gantry(self.var_state['curr']['gantry'])
        if not self.delta_trolley():
            self.trolley()
        if not self.delta_hoist():
            self.hoist()


def relevant_key(var):
    """
    Chck if var is in relevant keys for updating time
    :param var:
    :return:
    """
    return var in ["trolley", "hoist"]


def check_state_error(var):
    pass

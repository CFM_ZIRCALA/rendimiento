"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from App.connModule.connector import Connector as GeneralConnector
from App.logHandler.ErrorHandler import ErrorHandler


class Connector(GeneralConnector):
    def __init__(self, settings):
        super().__init__(settings)
        # gr_addr es para las direcciones de la grua..tal vez se puede parametrizar pero no hacen muchas ganas

        self.addr = settings.gr_addr
        self.types = {}
        for key in self.addr.keys():
            if key in settings.data_types.keys():
                self.types[key] = settings.data_types[key]
            else:
                self.types[key] = False

    def get_var(self, var):
        # print("")
        if var in self.addr.keys():
            if self.connect():
                # en caso de que la variable sea continua(True), asumo float y convierto antes de retornar
                if self.types[var]:
                    reg = self.client.read_holding_registers(int(self.addr[var]) * 2, 2)
                    if reg:
                        return self.to_float(reg)
                    else:
                        ErrorHandler(self.client.last_error(), 'gruaConnector read register',
                                     ' no return from reading register(double word):' + str(
                                         var) + " at addr: " + str(self.addr[var]))
                        return None
                else:
                    ret = self.client.read_discrete_inputs(int(self.addr[var]), 1)
                    if ret:
                        return ret[0]
                    else:
                        print(self.client.last_error())
                        ErrorHandler(self.client.last_error(), 'gruaConnector read discrete',
                                     ' no return from reading discrete input :' + str(
                                         var) + " at addr: " + str(self.addr[var]))
                        return None

            else:
                return ErrorHandler(self.client.last_error(), 'gruaConnector self.connect()',
                                    'No connection established')
        else:
            ErrorHandler(False, "gruaConnector: get_var", "Var: " + var + " doesn't exist in grua's declared variables")
            return -1

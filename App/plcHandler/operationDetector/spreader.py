"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from collections import deque
from time import time as clock

# Todo: tal vez arreglar este import de abajo
from App.logHandler.ErrorHandler import ErrorHandler as Errors
from App.plcHandler.operationDetector.spreaderConnector import Connector


class Spreader:
    """
    Clase que se encarga del manejo de los datos del spreader.
    """

    def __init__(self, settings):
        """
        Constructor de la clase, inicia una conexion, e inicia los tiempos en -1

        :param settings: Configuraciones cargadas desde config.ini
        """
        try:
            self.connection = Connector(settings)
            self.var_state = {'prev_3': {}, 'prev_2': {}, 'prev': {}, 'curr': {}}
            for key in settings.sp_addr:
                # Contains previous and curent values of the variables
                self.var_state["prev_3"][key] = -10
                self.var_state["prev_2"][key] = -10
                self.var_state["prev"][key] = -10
                self.var_state["curr"][key] = -10
            self.sp_free_t0 = -1  # Inicio tiempo destrabado
            self.sp_free_t1 = -1  # Fin tiempo destrabado
            self.sp_free_total = -1  # dif de marcas, valor del tiempo destrabado
            self.sp_eng_t0 = -1  # Inicio tiempo trabado
            self.sp_eng_t1 = -1  # Fin tiempo trabado
            self.sp_eng_total = -1  # Dif de marcas, valor del tiempo destrabado
            self.sp_temp = -1
            self.sp_last_mv = -1
            self.sp_curr_mv = -1
            self.container_type = ""
            self.locked = -1
            self.boom_t0 = -1
            self.boom_total = -1
            Errors("Spreader created")
            if self.connection.client is Errors:
                raise Errors
        except Errors:
            Errors("Fail create connector")
            self.connection = Errors

    def boom_start(self):
        if self.boom_t0 == -1:
            self.boom_t0 = clock()
            self.boom_total = -1

    def print_one(self):
        '''
        do not ever use this function, please.
        :return: 1
        '''
        print(1)
        return 1

    def boom_end(self):
        if self.boom_total == -1 and self.boom_t0 != -1:
            self.boom_total = abs(clock() - self.boom_t0)
            self.boom_t0 = -1

    def reset_times(self):
        self.sp_free_t0 = clock()  # Inicio tiempo destrabado
        self.sp_free_t1 = -1  # Fin tiempo destrabado
        self.sp_free_total = -1  # dif de marcas, valor del tiempo destrabado
        self.sp_eng_t0 = -1  # Inicio tiempo trabado
        self.sp_eng_t1 = -1  # Fin tiempo trabado
        self.sp_eng_total = -1  # Dif de marcas, valor del tiempo destrabado
        self.sp_curr_mv = -1
        self.sp_last_mv = -1

    def get_free_total(self):
        return self.sp_free_total

    def get_eng_total(self):
        return self.sp_eng_total

    def get_total_time(self):
        return self.get_eng_total() + self.get_free_total()

    def sp_last_mv(self):
        return self.sp_last_mv

    def curr_mv(self):
        return self.sp_curr_mv

    def container_type(self):
        return self.container_type

    def locked(self):
        return self.locked

    def get_state(self):
        self.refresh()
        if parse_state(self.var_state["curr"]) == "q1":
            self.locked = False
        elif parse_state(self.var_state["curr"]) == "q4":
            self.locked = True

    def get_container_type(self):
        if self.var_state["curr"]["twin"] != self.var_state["curr"]["single"]:
            if self.var_state["curr"]["twin"]:
                if self.var_state["curr"]["20feet"]:
                    Errors(False, "spreader: get_container_type",
                           "Error retrieving container type: twin mode and 20feet recognized")
                    return "error, 20 feet + twin"
                return "2x20"
            elif self.var_state["curr"]["single"]:
                if self.var_state["curr"]["20feet"]:
                    return "20"
                if self.var_state["curr"]["40feet"]:
                    return "40"
                if self.var_state["curr"]["45feet"]:
                    return "45"
        return "None"

    def lock(self):
        self.locked = True

    def unlock(self):
        self.locked = False

    def detect(self):
        """

        :return:
        """

        # time = clock()

        self.sp_curr_mv = self.get_movement()
        if self.sp_curr_mv != self.sp_temp:
            if self.sp_curr_mv is not None and self.sp_curr_mv != self.sp_last_mv and self.sp_curr_mv != -1:
                if self.sp_curr_mv == 1:
                    # 1 PARA ENGANCHE
                    container_type = self.get_container_type()
                    # Comienza a correr el cronometro
                    # Si venia de desenganchar, osea venia libre
                    if self.sp_last_mv == 2:
                        # Si venia libre con temporizador
                        if self.sp_free_t0 != -1:
                            self.sp_free_t1 = clock()
                            self.sp_free_total = self.sp_free_t1 - self.sp_free_t0
                            # Errors("Tiempo spreader libre/desenganchado: ", self.sp_free_total)
                        # Si venia sin movimiento previo
                        else:
                            self.sp_free_t0 = -1
                    self.sp_eng_t0 = clock()

                    # tipo = spreader.get_container_type()

                elif self.sp_curr_mv == 2:
                    # 2 PARA DESTRABADO
                    self.container_type = 0
                    if self.sp_last_mv == 1:
                        if self.sp_eng_t0 != -1:
                            self.sp_eng_t1 = clock()
                            self.sp_eng_total = self.sp_eng_t1 - self.sp_eng_t0
                            # Errors("Tiempo spreader enganchado: ", self.sp_eng_total)
                        else:
                            self.sp_eng_t0 = -1
                    self.sp_free_t0 = clock()

                Errors("Detected: ", self.sp_curr_mv)
                # Errors("Tiempo spreader libre/desenganchado: ", self.sp_free_total)
                # Errors("Tiempo spreader enganchado: ", self.sp_eng_total)
            if self.sp_curr_mv != -1 and self.sp_curr_mv is not None:
                self.sp_last_mv = self.sp_curr_mv
        self.sp_temp = self.sp_curr_mv

        # Errors("temp: ", self.sp_temp, "  curr: ", self.sp_curr_mv, "  last: ", self.sp_last_mv, "\n", "t libre: ",
        #       self.sp_free_total,
        #       "  T eng: ", self.sp_eng_total)
        # Errors("time: ", clock() - time)
        # if clock() - time > 1:
        #     sleep(3)
        #     Errors("demora!!")
        #           # ,self.var_state)

        # Errors(parse_state(self.var_state['curr']))

    def refresh(self):
        """
        Refresca/actualiza los momentos almacenados

        :return: True|False si reconoce un tiempo valido y distinto al actual
        """
        temp = {}
        flag = False  # flag avisa si es valido copiar o no el temporal al curr, y desplazar los anteriores
        for key in self.var_state["curr"].keys():
            temp[key] = self.get_var_state(key)
            # Si es una de las variables que no deberia influir en la
            # secuencia de estados para el enganche, se salta la comprobacion
            if relevant_key(key):
                if temp[key] == -1:
                    flag = False
                    break
                elif temp[key] != self.var_state['curr'][key]:
                    flag = True
        # Si encuentra un una diferencia entre el estado actual de las variables,
        # y lo leido al refrescar, almacena el estado y desplaza los anteriores
        # -1 se usa como segnal de error, por lo que si lo encuentra no actualiza

        # Si es valido copiar, actualizo los valores
        # if|
        if check_state_error(temp, "refresh"):
            flag = False
        if flag is True:
            self.var_state["prev_3"] = self.var_state["prev_2"].copy()
            self.var_state["prev_2"] = self.var_state["prev"].copy()
            self.var_state["prev"] = self.var_state["curr"].copy()
            self.var_state["curr"] = temp.copy()
        return flag

    def get_movement(self):
        """
        Recupera y reconoce el movimiento que ejecuta el spreader

        :return: codigo de movimiento,
                1->Enganche
                2->Desenganche
                -1-> no reconocible |error
        """
        if self.refresh() is False:
            # Todo: write backup or sp_sequence
            # Errors(False, "spreader get_movement()", "Failed to refresh state")
            return -1
        # Compraro tu y tl en el tiempo actual estados(tiempos)
        if check_state_error(self.var_state['curr']):
            return -1
        else:
            err_prev_3 = False  # Al parecer este caso no sirve de nada ja ja
            err_prev_2 = False
            # q1 : Land = 0 -> TU=1, TL=0
            # q2 : Land = 1 -> TU=1, TL=0
            # q3 : Land = 1 -> TU=0, TL=1
            # q4 : Land = 0 -> TU=0, TL=1

            # Reviso tiempos/estados validos
            for key in self.var_state["prev_3"].keys():
                if -1 == self.var_state["prev_3"][key]:
                    err_prev_3 = True
                if -1 == self.var_state["prev_2"][key]:
                    err_prev_2 = True
                break
            # Revisar cantidad de estados ingresados/validos
            if err_prev_3:
                # algoritmo cuando solo se trabaja con dos estados, detectar paso de Q2->Q1 || Q3->Q4
                if parse_state(self.var_state['curr']) == "q1":
                    self.unlock()
                    return 2
                elif parse_state(self.var_state['curr']) == "q4":
                    self.lock()
                    return 1

            if parse_state(self.var_state['prev_3']) == "q1":
                if parse_state(self.var_state['prev_2']) == "q2":
                    if parse_state(self.var_state['prev']) == "q3":
                        if parse_state(self.var_state['curr']) == "q4":
                            self.lock()
                            return 1
            # Si se pone a trabar y destrabar

            elif parse_state(self.var_state['prev_3']) == "q4":
                if parse_state(self.var_state['prev_2']) == "q3":
                    if parse_state(self.var_state['prev']) == "q2":
                        if parse_state(self.var_state['curr']) == "q1":
                            self.unlock()
                            return 2
            elif self.sp_last_mv == 2 and parse_state(self.var_state["curr"]) == "q4" and not self.locked:
                self.lock()
                return 1
            elif self.sp_last_mv == 1 and parse_state(self.var_state["curr"]) == "q1" and self.locked:
                self.unlock()
                return 2
            # Cuando hay 4 estados pero no se ha cumplido la secuencia completa
            elif self.sp_last_mv == -1 and parse_state(self.var_state["curr"]) == 'q4':
                self.lock()
                return 1

            elif self.sp_last_mv == -1 and parse_state(self.var_state["curr"]) == 'q1':
                self.unlock()
                return 2

    def get_var_state(self, var):
        put = self.connection.get_var(var)
        if put is not Errors and not None:
            if var in self.var_state['curr'].keys():
                return put
            else:
                Errors(None, "spreader: get_var_state", "var:" + var + " not in var_sate[curr] keys")
                return -1
        else:
            return -1


def check_state_error(var, call=""):
    """

    :param var: dict
        Diccionario sobre el que se compara tu==tl

    :param call: string
        Opcional, variable que indica la llamada para filtrar algunos casos de error
    :return: True si existe error
    """

    if var['tl'] == -10 or var["tu"] == -10:
        Errors("iniciando")
        return True
    if var['tl'] is True and var['tu'] is True:
        Errors(False, " spreader check_state_error", "TL==TU==TRUE, check signal issues or addr missconfiguration")
        Errors("TL == TU")
        return True
    if call == "refresh":
        if var["tl"] is False and var["tu"] is False:
            return True

    return False


def parse_state(var):
    """
    Funcion que traduce el estado del momento que se ingresa

    :param var: momento
    :return: estado q_n del momento
    """
    # q1 : Land = 0 -> TU=1, TL=0
    # q2 : Land = 1 -> TU=1, TL=0
    # q3 : Land = 1 -> TU=0, TL=1
    # q4 : Land = 0 -> TU=0, TL=1
    if not var['land'] and var['tu']:
        return 'q1'
    if var['land'] and var['tu']:
        return 'q2'
    if var['land'] and not var['tu']:
        return 'q3'
    if not var['land'] and not var['tu']:
        return 'q4'
    return Errors


def relevant_key(key):
    """

    :param key: Clave a comparar si es relevante
    :return: True|False si la clave es o no relevante
    """
    if key in ["tu", "tl", "land"]:
        return True
    return False

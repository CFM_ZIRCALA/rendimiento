"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from threading import Thread
from queue import Queue
from App.GUI import GUI
from PyQt5 import QtCore, QtWidgets, QtGui
import sys
import os
from App.plcHandler.plc import Handler as PLC
import time
import RPi.GPIO as GPIO
from time import sleep


class Tool:
    def __init__(self):
        sleep(1)
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
        LOG_DIR_PATH = os.path.join(ROOT_DIR, 'logs')

        if not os.path.exists(LOG_DIR_PATH):
            os.mkdir(LOG_DIR_PATH)
        LOG_PATH = os.path.join(LOG_DIR_PATH, time.strftime('%Y-%m-%d') + '.log')
        sys.stdout = open(LOG_PATH, 'a')
        print("**********Log start**********")
        gui_queue = Queue()
        app = QtWidgets.QApplication(sys.argv)
        plc = PLC(gui_queue, ROOT_DIR, LOG_DIR_PATH)
        plc.daemon = True
        window = GUI.GUI(gui_queue)
        print("Detector ready")
        plc.start()
        window.show()
        print('**Gui starting**')
        app.exec_()
        GPIO.cleanup()

"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from PyQt5 import QtCore, QtWidgets, QtGui
from App.GUI.Gui_code import Ui_MainWindow
from App.GUI.resources import *
from time import sleep, strptime
from datetime import datetime

from queue import Queue, Empty


class GUI(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, gui_queue):
        super().__init__()
        self.setupUi(self)
        self.Id_Label.setText('')
        self.guiThread = GuiThread(gui_queue)
        self.guiThread.emb20.connect(self.emb20)
        self.guiThread.demb20.connect(self.demb20)
        self.guiThread.emb20_oh.connect(self.emb20_oh)
        self.guiThread.demb20_oh.connect(self.demb20_oh)
        self.guiThread.tot20.connect(self.tot20)
        self.guiThread.emb2x20.connect(self.emb2x20)
        self.guiThread.demb2x20.connect(self.demb2x20)
        self.guiThread.tot2x20.connect(self.tot2x20)
        self.guiThread.emb40.connect(self.emb40)
        self.guiThread.demb40.connect(self.demb40)
        self.guiThread.emb40_oh.connect(self.emb40_oh)
        self.guiThread.demb40_oh.connect(self.demb40_oh)
        self.guiThread.tot40.connect(self.tot40)
        self.guiThread.emb45.connect(self.emb45)
        self.guiThread.demb45.connect(self.demb45)
        self.guiThread.tot45.connect(self.tot45)
        self.guiThread.embTap.connect(self.embTap)
        self.guiThread.dembTap.connect(self.dembTap)
        self.guiThread.totTap.connect(self.totTap)
        self.guiThread.totTot.connect(self.totTot)
        self.guiThread.reset.connect(self.reset)
        self.guiThread.log_text.connect(self.log_text)
        self.guiThread.start()

    def log_text(self, text):
        self.LastMV_Text.append(text)

    # @QtCore.pyqtSlot(str, str, str, str)
    def reset(self, id, name, start, crane):
        print(crane, str(crane))
        self.Id_Label.setText(id)
        self.Nombre_Label.setText(name)
        self.InicioTurno_Label.setText(start)
        self.NumeroGrua_Label.setText(crane)

        self.rendimiento_mv_hora_LCD.display(0)
        self.rendimiento_fallas_LCD.display(0)
        self.rendimiento_tiempo_esp_LCD.display(0)
        self.rendimiento_tiempo_op_LCD.display(0)

        self.emb20_LCD.display(0)
        self.demb20_LCD.display(0)

        self.emb20_OH_LCD.display(0)
        self.demb20_OH_LCD.display(0)

        self.emb2x20_LCD.display(0)
        self.demb2x20_LCD.display(0)

        self.emb2x20_LCD.display(0)
        self.demb2x20_LCD.display(0)

        self.emb40_LCD.display(0)
        self.demb40_LCD.display(0)

        self.emb40_OH_LCD.display(0)
        self.demb40_OH_LCD.display(0)

        self.emb45_LCD.display(0)
        self.demb45_LCD.display(0)

        self.embTap_LCD.display(0)
        self.dembTap_LCD.display(0)
        self.update_totals()

    # @QtCore.pyqtSlot(int)
    def emb20(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.emb20_LCD.display(self.emb20_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def demb20(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.demb20_LCD.display(self.demb20_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    def emb20_oh(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.emb20_OH_LCD.display(self.emb20_OH_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def demb20_oh(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.demb20_OH_LCD.display(self.demb20_OH_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def tot20(self, val):
        self.tot20_LCD.display(val)

    # @QtCore.pyqtSlot(int)
    def emb2x20(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.emb2x20_LCD.display(self.emb2x20_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def demb2x20(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.demb2x20_LCD.display(self.demb2x20_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def tot2x20(self, val):
        self.tot2x20_LCD.display(val)

    # @QtCore.pyqtSlot(int)
    def emb40(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.emb40_LCD.display(self.emb40_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def demb40(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.demb40_LCD.display(self.demb40_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    def emb40_oh(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.emb40_OH_LCD.display(self.emb40_OH_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def demb40_oh(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.demb40_OH_LCD.display(self.demb40_OH_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def tot40(self, val):
        self.tot40_LCD.display(val)

    # @QtCore.pyqtSlot(int)
    def emb45(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.emb45_LCD.display(self.emb45_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def demb45(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.demb45_LCD.display(self.demb45_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def tot45(self, val):
        self.tot45_LCD.display(val)

    # @QtCore.pyqtSlot(int)
    def embTap(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.embTap_LCD.display(self.embTap_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def dembTap(self, hora, mov, carga, tiempo_op, tiempo_esp):
        self.dembTap_LCD.display(self.dembTap_LCD.intValue() + 1)
        self.update_totals()
        self.last_mv(hora, mov, carga, tiempo_op, tiempo_esp)

    # @QtCore.pyqtSlot(int)
    def totTap(self, val):
        self.totTap_LCD.display(val)

    # @QtCore.pyqtSlot(int)
    def totTot(self, val):
        self.totTot_LCD.display(val)

    def update_totals(self):
        self.tot20_LCD.display(self.emb20_LCD.intValue() + self.demb20_LCD.intValue())
        self.tot20_OH_LCD.display(self.emb20_OH_LCD.intValue() + self.demb20_OH_LCD.intValue())
        self.tot40_LCD.display(self.emb40_LCD.intValue() + self.demb40_LCD.intValue())
        self.tot40_OH_LCD.display(self.emb40_OH_LCD.intValue() + self.demb40_OH_LCD.intValue())
        self.tot45_LCD.display(self.emb45_LCD.intValue() + self.demb45_LCD.intValue())
        self.tot2x20_LCD.display(self.emb2x20_LCD.intValue() + self.demb2x20_LCD.intValue())
        self.totTap_LCD.display(self.embTap_LCD.intValue() + self.dembTap_LCD.intValue())

        self.totTot_LCD.display(
            self.tot20_LCD.intValue() + 2 * self.tot2x20_LCD.intValue() + self.tot40_LCD.intValue() +
            self.tot45_LCD.intValue() + self.tot20_OH_LCD.intValue() +
            self.tot40_OH_LCD.intValue())

    def last_mv(self, hora, mov, carga, tiempo_op, tiempo_esp):
        if tiempo_esp == -1:
            tiempo_esp = 0

        rend = (datetime.now() - datetime.strptime(self.InicioTurno_Label.text(), '%H:%M %d/%m/%Y')).total_seconds() / (
                60 * 60)
        # Lo calculo completo de nuevo ya que el total de movimientos difiere al calcular el rendimiento gracias a los 2x20
        rend = (
                           self.tot20_LCD.intValue() + self.tot20_OH_LCD.intValue() + self.tot40_OH_LCD.intValue() + self.tot40_LCD.intValue() + self.tot45_LCD.intValue() + self.tot2x20_LCD.intValue()) / rend

        rend = float('%.2f' % (rend))
        self.rendimiento_mv_hora_LCD.display(rend)
        if mov.find('falla') != -1:
            self.rendimiento_fallas_LCD.display(self.rendimiento_fallas_LCD.intValue())
        self.last_mv_hora.setText(datetime.strftime(datetime.now(), '%H:%M:%S'))
        self.last_mv_mv.setText(mov)
        self.last_mv_tipo.setText(carga)
        self.last_mv_tiempoEsp.setText(str(tiempo_esp))
        self.last_mv_tiempoOp.setText(str(tiempo_op))

        self.rendimiento_tiempo_esp_LCD.display(self.rendimiento_tiempo_esp_LCD.value() + (tiempo_esp / 60))
        self.rendimiento_tiempo_op_LCD.display(self.rendimiento_tiempo_op_LCD.value() + (tiempo_op / 60))


class GuiThread(QtCore.QThread):
    emb20 = QtCore.pyqtSignal(float, str, str, float, float)

    demb20 = QtCore.pyqtSignal(float, str, str, float, float)

    emb20_oh = QtCore.pyqtSignal(float, str, str, float, float)

    demb20_oh = QtCore.pyqtSignal(float, str, str, float, float)

    tot20 = QtCore.pyqtSignal(int)

    emb2x20 = QtCore.pyqtSignal(float, str, str, float, float)

    demb2x20 = QtCore.pyqtSignal(float, str, str, float, float)

    tot2x20 = QtCore.pyqtSignal(int)

    emb40 = QtCore.pyqtSignal(float, str, str, float, float)

    demb40 = QtCore.pyqtSignal(float, str, str, float, float)

    emb40_oh = QtCore.pyqtSignal(float, str, str, float, float)

    demb40_oh = QtCore.pyqtSignal(float, str, str, float, float)

    tot40 = QtCore.pyqtSignal(int)

    emb45 = QtCore.pyqtSignal(float, str, str, float, float)

    demb45 = QtCore.pyqtSignal(float, str, str, float, float)

    tot45 = QtCore.pyqtSignal(int)

    embTap = QtCore.pyqtSignal(float, str, str, float, float)

    dembTap = QtCore.pyqtSignal(float, str, str, float, float)

    totTap = QtCore.pyqtSignal(int)

    totTot = QtCore.pyqtSignal(int)

    reset = QtCore.pyqtSignal(str, str, str, str)
    log_text = QtCore.pyqtSignal(str)

    def __init__(self, in_queue):
        super(GuiThread, self).__init__()
        self.queue = in_queue
        print(type(self.queue), type(in_queue), 'GUIth')

    def run(self):
        opt = {}
        while True:
            try:
                opt = self.queue.get(timeout=1)
                if opt[0] == "emb20":
                    self.emb20.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "demb20":
                    self.demb20.emit(opt[1], opt[2], opt[3], opt[4], opt[5])

                elif opt[0] == "demb20-OH":
                    self.demb20_oh.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "demb20-OH":
                    self.demb20_oh.emit(opt[1], opt[2], opt[3], opt[4], opt[5])

                elif opt[0] == "tot20":
                    self.tot20.emit(0)
                elif opt[0] == "emb2x20":
                    self.emb2x20.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "demb2x20":
                    self.demb2x20.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "tot2x20":
                    self.tot2x20.emit(0)

                elif opt[0] == "emb40":
                    self.emb40.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "demb40":
                    self.demb40.emit(opt[1], opt[2], opt[3], opt[4], opt[5])

                elif opt[0] == "emb40-OH":
                    self.emb40_oh.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "demb40-OH":
                    self.demb40_oh.emit(opt[1], opt[2], opt[3], opt[4], opt[5])

                elif opt[0] == "tot40":
                    self.tot40.emit(0)
                elif opt[0] == "emb45":
                    self.emb45.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "demb45":
                    self.demb45.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "tot45":
                    self.tot45.emit(0)
                elif opt[0] == "embtapa":
                    self.embTap.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "dembtapa":
                    self.dembTap.emit(opt[1], opt[2], opt[3], opt[4], opt[5])
                elif opt[0] == "totTap":
                    self.totTap.emit(0)
                elif opt[0] == "totTot":
                    self.totTot.emit(0)
                elif opt[0] == "log":
                    self.log_text.emit(str(opt[1]))
                elif opt[0] == "session":
                    # Opt del 1 al 4 deben ser id, nombre, inicio de turno, numero de grua
                    self.reset.emit(str(opt[1]), str(opt[2]), str(opt[3]), str(opt[4]))
            except Empty:
                sleep(0.2)
            finally:
                opt = -1

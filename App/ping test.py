"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
#!/usr/bin/env python

from time import sleep

# from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pyModbusTCP.client import ModbusClient


def test():
    n = input()
    client = ModbusClient(host='192.168.129.80', debug=True)
    print("Client: " + str(client))
    print("Client Mode: " + str(client.mode()))
    print("TCP conn status(L10): " + str(client.is_open()))
    print("unit id: " + str(client.unit_id()))

    # Sirve con pyModbus lib, no modbusTCP
    # con=client.connect()
    if not client.is_open():
        print("trying to open client //open connection")
        client.open()
    if client.is_open():
        it = 0
        while True:
            try:
                client.open()
                it += 1
                print("\n*****iteration: " + str(it) + "*****")
                print("TCP conn status: " + str(client.is_open()))
                print("Port: " + str(client.port()))
                print("Unit ID: " + str(client.unit_id()))
                print("Port: " + str(client.port()))

                print("\n----start----\nReg_hold: ")
                reg_hold = client.read_holding_registers(0, 20)
                # print("response: " + str(reg_hold) + "\n--------\n")
                # dir = 32768
                # print("reg2: dir=" + str(dir))
                #  print("response: " + str(reg_hold) + "\n--------\n")
                # #reg_hold = client.read_holding_registers(dir, 20)
                # p
                # print("reg inp: ")
                reg_input = client.read_input_registers(0, 20)
                print("response: " + str(reg_input) + "\n--------\n")

                print("disc inp: ")
                reg_discrete = client.read_discrete_inputs(0, 100)
                print("response: " + str(reg_discrete) + "\n--------\n")
                print("**iteration: " + str(it) + "***")
                # print("Coils: ")
                # reg_coil=0

                sleep(float(n))

            except KeyboardInterrupt:
                print("Conn closed\n-" + str(client.close()))
                break
    else:
        print("no conn")
    if not client.is_open():
        print("client closed")
    print("--test end--")


test()

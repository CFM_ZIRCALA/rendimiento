import RPi.GPIO as GPIO
import time


class Led():
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(7, GPIO.OUT)
        GPIO.output(7, False)

    def success(self):
        GPIO.output(7, True)
        time.sleep(5)
        GPIO.output(7, False)

    def fail(self):
        for i in range(1, 5):
            GPIO.output(7, True)
            time.sleep(0.25)
            GPIO.output(7, False)
            time.sleep(0.25)



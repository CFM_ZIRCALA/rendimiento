#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Por Reynaldo Palomino para StiPort @2019
<reynaldo.palomino.92@gmail.com>
"""

import os
import led
from queue import Queue
import hashlib
import time
import listener
from pyfingerprint.pyfingerprint import PyFingerprint

try:
    green_led = led.Led()

    ## Creates the listener thread as well as communication queues
    op_q = Queue()
    index_q = Queue()
    template_q = Queue()
    response_q = Queue()
    thread = listener.ListenerThread(op_q=op_q, index_q=index_q, template_q=template_q, response_q=response_q)
    thread.daemon = True
    thread.start()
except Exception as e:
    print('Exception message: ' + str(e))
    exit(0)

while True:
    ## Tries to initialize the sensor
    error = False
    for i in range(0, 4):
        try:
            f = PyFingerprint('/dev/ttyUSB' + str(i), 57600, 0xFFFFFFFF, 0x00000000)

            if (f.verifyPassword() == False):
                raise ValueError('The given fingerprint sensor password is wrong!')
            else:
                break

        except Exception as e:
            print('Exception message: ' + str(e))
            time.sleep(1)
            if i == 3:
                print('The fingerprint sensor could not be initialized, will keep retrying!')
                error = True
    if error == True:
        continue

    ## Gets some sensor information
    print('Currently used templates: ' + str(f.getTemplateCount()) + '/' + str(f.getStorageCapacity()))
    lastHash = -1

    while True:

        ## Tries to search the finger and calculate hash
        try:
            print('Waiting for finger...')

            ## Wait that finger is read
            while (f.readImage() == False):
                if not op_q.empty():
                    op = op_q.get()
                    if op == 'enroll':
                        ## Retrieves data received by the listener thread and stores it in the sensor      
                        if not index_q.empty() and not template_q.empty():
                            try:
                                index = index_q.get()
                                completePayload = template_q.get()
                                f.uploadCharacteristics(0x01, completePayload)
                                f.storeTemplate(int(index), 0x01)
                                response_q.put('success')
                                print('New fingerprint stored')
                            except Exception as e:
                                print('Exception message: ' + str(e))
                                response_q.put('fail')
                    elif op == 'delete':
                        if not index_q.empty():
                            try:
                                index = index_q.get()
                                f.deleteTemplate(int(index))
                                print('Fingerprint deleted | index = ' + str(index))
                                response_q.put('success')
                            except Exception as e:
                                print('Exception message: ' + str(e))
                                response_q.put('fail')

                    else:
                        print('Unknown operation requested from local thread')
                        response_q.put('fail')

            ## Converts read image to characteristics and stores it in charbuffer 1
            f.convertImage(0x01)

            ## Searchs template
            result = f.searchTemplate()
            positionNumber = result[0]

            if (positionNumber == -1):
                print('Huella no reconocida, inténtelo de nuevo')
                green_led.fail()
                while (f.readImage() == True):
                    pass
            else:
                file = open('import.fpnt.tmp', 'w+')
                file.write(str(positionNumber))
                file.close()
                os.rename('import.fpnt.tmp', 'import.fpnt')

                ## Loads the found template to charbuffer 1
                f.loadTemplate(positionNumber, 0x01)

                ## Downloads the characteristics of template loaded in charbuffer 1
                characterics = str(f.downloadCharacteristics(0x01)).encode('utf-8')

                ## Hashes characteristics of template
                lastHash = hashlib.sha256(characterics).hexdigest()
                print('SHA-2 hash of template: ' + lastHash)
                print('Retire su huella por favor')
                green_led.success()
                while (f.readImage() == True):
                    pass

        except Exception as e:
            print('exception')
            if str(e) == 'Unknown error 0x17':
                while (f.readImage() == True):
                    pass
            else:
                print('Operation failed!')
                print('Exception message: ' + str(e))
                break
thread.join()

"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
from datetime import datetime
from time import time


class ErrorHandler(Exception):
    def __init__(self, error="", call="", msj=""):
        super(ErrorHandler, self).__init__(msj)
        date = datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S')
        print('[' + date + ']: ', error, "  ", call, "  ", msj, )

import os, time
import threading
from queue import Queue
import socket
import json
import crane_operator

"""
Por Reynaldo Palomino para StiPort @2019
<reynaldo.palomino.92@gmail.com>
"""


class ListenerThread(threading.Thread):

    def __init__(self, op_q, index_q, template_q, response_q):
        super(ListenerThread, self).__init__()
        self.op_q = op_q
        self.index_q = index_q
        self.template_q = template_q
        self.response_q = response_q
        self.stoprequest = threading.Event()

    def send(self, socket, data):
        for i in range(0, 20):
            try:
                socket.sendall(data.encode('utf-8'))
                break
            except:
                pass

    def receive(self, socket, data_size):
        for i in range(0, 20):
            try:
                data = socket.recv(data_size).decode()
                
                Payload = False
                if data[0] == '[':
                    Payload = True
                
                while Payload and data[-1:] != ']':
                    data += socket.recv(data_size).decode()
                self.send(socket, 'success')
                return data
            
            except Exception as e:
                print(str(e))
                continue
        self.send(socket, 'fail')

    def run(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            host = socket.getfqdn()
            port = 6660
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            while not self.stoprequest.isSet():
                try:
                    s.bind(('192.168.100.36', port))
                    print('Successful binding to port ' + str(port))
                    s.listen(2)
                    while not self.stoprequest.isSet():
                        try:
                            print('Waiting for connection')
                            conn, addr = s.accept()
                            print('Conecction accepted')
                            conn.settimeout(1)
                            
                            operation = self.receive(conn, 4096)
                            if operation == 'enroll':
                                
                                index = int(self.receive(conn, 4096))
                                templateText = self.receive(conn, 4096)
                                template = list( map(int, templateText.split('[')[1].split(']')[0].split(',')) )
                                name = self.receive(conn, 4096)
                                rut = self.receive(conn, 4096)
                                
                                self.op_q.put(operation)
                                self.index_q.put(index)
                                self.template_q.put(template)

                                crane_operatorr = crane_operator.Operator(rut, name, [index])
                                crane_operatorr.save()
                                
                            elif operation == 'delete':
                                index = self.receive(conn, 4096)
                                self.op_q.put(operation)
                                self.index_q.put(index)
                                
                                crane_operatorr = crane_operator.Operator()
                                crane_operatorr.complete_operator(index)
                                crane_operatorr.delete()
                            else:
                                print('Unknown operation requested from connection')
                                self.send(conn, 'fail')

                            for i in range(0, 200):
                                if not self.response_q.empty():
                                    response = self.response_q.get()
                                    if response == 'success':
                                        print('Sending back \'success\' signal')
                                        self.send(conn, 'success')
                                        break
                                    if response == 'fail':
                                        print('should send fail :(')
                                        self.send(conn, 'fail')
                                        break
                                time.sleep(0.1)
                        except Exception as e:
                            print('Exception message: ' + str(e) + ', retrying...')
                            time.sleep(2)
                except Exception as e:
                    print('Exception message: ' + str(e) + ', retrying...')
                    time.sleep(2)
        except Exception as e:
            print('Operation failed!')
            print('Exception message: ' + str(e))
        s.close()

    def join(self, timeout=None):
        self.stoprequest.set()
        super(ListenerThread, self).join(timeout)

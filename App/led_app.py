"""
author: David Fuentes
mail: david.fuentesalvear@gmail.com
@2019 to STIport
"""
import threading
import queue
import time
import RPi.GPIO as GPIO


class Led(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(15, GPIO.OUT)  # Led de sesion
        GPIO.setup(13, GPIO.OUT)  # Led de aplicacion
        GPIO.output(15, False)
        GPIO.output(13, False)
        self.queue = queue
        self.anon = -1

    def run(self):
        print("Led started running")
        GPIO.output(13, True)  # Prendo el led si la aplicacion corre
        while True:
            try:
                self.anon = self.queue.get(timeout=1)

                if self.anon is True:
                    GPIO.output(15, False)
                    GPIO.output(15, True)
                    time.sleep(0.5)
                    GPIO.output(15, False)
                    time.sleep(0.25)
                elif self.anon is False:
                    GPIO.output(15, True)
                elif self.anon == -1:
                    GPIO.output(15, False)
            except queue.Empty:
                if self.anon is True:
                    GPIO.output(15, False)
                    GPIO.output(15, True)
                    time.sleep(0.5)
                    GPIO.output(15, False)
                    time.sleep(0.25)
                elif self.anon == -1:
                    GPIO.output(15, False)



    @staticmethod
    def clean():
        GPIO.cleanup()

    # def app_kill(self):
    #     self.anon = False
    #     GPIO.cleanup()
    #
    # def start_anon(self):
    #     # self.anon = False
    #     while True:
    #         if self.anon:
    #             GPIO.output(15, True)
    #             time.sleep(0.5)
    #             GPIO.output(15, False)
    #             time.sleep(0.25)
    #         else:
    #             break
    #
    # def start_id(self):
    #     self.anon = False
    #     GPIO.output(15, True)
    #
    # @staticmethod
    # def end_id():
    #     GPIO.output(15, False)
    #     time.sleep(1)
